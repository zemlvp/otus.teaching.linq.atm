﻿using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        private static void Main()
        {
            System.Console.WriteLine("Старт приложения-банкомата...\n");

            var atmManager = CreateATMManager();

            #region 1. Вывод информации о заданном аккаунте по логину и паролю
            WriteLineConsoleGreen("1. Вывод информации о заданном аккаунте по логину и паролю");

            var user = atmManager.GetUser("cant", "333");
            System.Console.WriteLine(user);
            #endregion

            #region 2. Вывод данных о всех счетах заданного пользователя
            WriteLineConsoleGreen("2. Вывод данных о всех счетах заданного пользователя");
            
            var accounts = atmManager.GetUserAccountsByLogin("snow");
            foreach (var a in accounts)
            {
                System.Console.WriteLine(a);
            }
            #endregion

            #region 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
            WriteLineConsoleGreen("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту: ShowUserAccountsHistory(\"snow\")");

            var accountHistories = atmManager.GetUserAccountsHistoryByLogin("snow");
            foreach (var h in accountHistories)
            {
                System.Console.WriteLine(
                    $"AccountId={h.AccountId}\n" +
                    $"OpeningDate={h.OpeningDate}\n" +
                    $"CashAll={h.CashAll}\n" +
                    $"OperationType={h.OperationType}\n" +
                    $"CashSum={h.CashSum}\n");
            }
            #endregion

            #region 4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
            WriteLineConsoleGreen("\n4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");

            var operationHistories = atmManager.GetAllInputOperationHistory();
            foreach (var h in operationHistories)
            {
                System.Console.WriteLine(
                    $"OperationHistoryId={h.OperationsHistory.Id}\n"+
                    $"AccountId={h.Account.Id}\n" +
                    $"CashAll={h.Account.CashAll}\n" +
                    $"OpeningDate={h.Account.OpeningDate.ToString("g")}\n" +
                    $"CashAll={h.Account.CashAll}\n" +
                    $"CashSum={h.OperationsHistory.CashSum}\n" +
                    $"FIO={h.User.SurName} {h.User.MiddleName} {h.User.FirstName}\n" +
                    $"Phone={h.User.Phone}\n" +
                    $"PassportSeriesAndNumber={h.User.PassportSeriesAndNumber}\n");
            }
            #endregion

            #region 5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
            WriteLineConsoleGreen("\n5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)");
            var users1 = atmManager.GetAllUserWhoCashGreaterThen(100499);
            foreach (var u in users1)
            {
                System.Console.WriteLine(u);
            }
            #endregion

            System.Console.WriteLine("\nЗавершение работы приложения-банкомата...");
            System.Console.ReadKey();
        }

        private static void WriteLineConsoleGreen(string text)
        {
            System.Console.ForegroundColor = System.ConsoleColor.Green;
            System.Console.WriteLine(text);
            System.Console.ResetColor();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}