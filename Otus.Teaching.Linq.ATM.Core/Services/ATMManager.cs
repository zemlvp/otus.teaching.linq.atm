﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        // Вывод информации о заданном аккаунте по логину и паролю
        public User GetUser(string login, string password)
        {
            return Users.Where(u => u.Login == login && u.Password == password).SingleOrDefault();
        }

        // Вывод данных о всех счетах заданного пользователя
        public IEnumerable<Account> GetUserAccountsByLogin(string login)
        {
            var accounts = from a in Accounts
                           join u in Users on a.UserId equals u.Id
                           where u.Login == login
                           select a;

            return accounts;
        }

        // Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        public IEnumerable<AccountHistory> GetUserAccountsHistoryByLogin(string login)
        {
            var accountHistories = from oh in History
                                   join a in Accounts on oh.AccountId equals a.Id
                                   join u in Users on a.UserId equals u.Id
                                   where u.Login == login
                                   orderby a.Id, a.OpeningDate descending
                                   select new AccountHistory
                                   {
                                       AccountId = oh.AccountId,
                                       OpeningDate = a.OpeningDate,
                                       CashAll = a.CashAll,
                                       OperationType = oh.OperationType == OperationType.InputCash ? "пополнение счета" : "снятие со счета",
                                       CashSum = oh.CashSum
                                   };

            return accountHistories;
        }

        // Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        public IEnumerable<OperationHistoryInfo> GetAllInputOperationHistory()
        {
            var operationHistories = from oh in History
                                     join a in Accounts on oh.AccountId equals a.Id
                                     join u in Users on a.UserId equals u.Id
                                     where oh.OperationType == OperationType.InputCash
                                     orderby oh.Id 
                                     select new OperationHistoryInfo
                                     {
                                         Account = a,
                                         OperationsHistory = oh,
                                         User = u
                                     };

            return operationHistories;
        }

        // Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        public IEnumerable<User> GetAllUserWhoCashGreaterThen(int n)
        {
            var allUsers = new List<User>();

            var groupedUsers = from a in Accounts
                               join u in Users on a.UserId equals u.Id
                               where a.CashAll > n
                               group u by u.Id into g
                               select new { Users = from u in g select u };

            foreach (var group in groupedUsers)
            {
                foreach (var user in group.Users)
                    allUsers.Add(user);
            }

            return allUsers;
        }
    }

    public class AccountHistory
    {
        public int AccountId { get; set; }
        public DateTime OpeningDate { get; set; }
        public decimal CashAll { get; set; }
        public string OperationType { get; set; }
        public decimal CashSum { get; set; }
    }

    public class OperationHistoryInfo
    {
        public Account Account { get; set; }
        public OperationsHistory OperationsHistory { get; set; }
        public User User { get; set; }

    }
}